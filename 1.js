var a=["azul","amarillo","rojo","verde","gris","blanco","negro","otorrinolaringologo"];
var aleatorio=Math.floor(Math.random()*(a.length)+0);
var palabra="";
palabra=a[aleatorio];
var arrayPalabra=palabra.split("");
var tamanoPalabra=arrayPalabra.length;
console.log(arrayPalabra,tamanoPalabra);
var fallos=[];
var cuentaAcertados=0;
var cuentaFallos=0;
var segundos=60;
function crearCajas(num){
	for(var i=0;i<num;i++){
		document.querySelector("header").innerHTML+='<div class="cajaLetras"></div>';
	}
	cuentaAtras=setInterval(function(){
		segundos--;
		document.querySelector(".contador").innerHTML=segundos;
		if(segundos==0){
			clearInterval(cuentaAtras);
			alert("¡Has perdido!, inténtalo otra vez");
			location.reload();
		}
	},1000);
}
function comprobarLetra(v){
	var letraIntroducida=document.querySelector("#cajaLetra").value;
	var contador=0;
	for(var i=0;i<v.length;i++){
		if(letraIntroducida==v[i]){
			document.querySelector(".acertados").innerHTML="Has acertado: "+letraIntroducida;
			var arrayDivs=document.querySelectorAll("header div");
			arrayDivs[i].innerHTML=v[i];
			cuentaAcertados++;
			if(cuentaAcertados==v.length){
				window.alert("¡Enhorabuena has acertado la palabra completa!");
				window.location.reload();
			}

		}else{
			contador++;
			if(contador==v.length){
				fallos.push(letraIntroducida);
				document.querySelector(".fallos").innerHTML="Has fallado, no hay letra: "+letraIntroducida;
				document.querySelector(".fallos").innerHTML+="<br> Letras Falladas: "
				document.querySelector(".fallos").innerHTML+=fallos;
				cuentaFallos++;
				document.querySelectorAll(".ahorcado>img")[cuentaFallos-1].style.display = 'block';
				setTimeout(function(){
					if(cuentaFallos==10){
						alert("¡Has perdido!, inténtalo otra vez");
						location.reload();
					}
				}, 1500);
			}
		}
	}	
}



